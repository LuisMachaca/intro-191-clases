#include <iostream>
using namespace std;

//Problema 1
void prob01() {
	int nota1, nota2, nota3;
	cout << "Ingrese nota PC1: ";
	cin >> nota1;

	cout << "Ingrese nota PC2: ";
	cin >> nota2;

	cout << "Ingrese nota PC3: ";
	cin >> nota3;

	float promedio = 1.0 * (nota1 + nota2 + nota3) / 3;
	cout << "Promedio: " << promedio;
}

//Problema 2
void prob02() {
	int nota1, nota2, nota3, nota4;
	int menor = 0;

	cout << "Ingrese nota PC1: ";
	cin >> nota1;
	menor = nota1;

	cout << "Ingrese nota PC2: ";
	cin >> nota2;
	if (menor > nota2) {
		menor = nota2;
	}

	cout << "Ingrese nota PC3: ";
	cin >> nota3;
	if (menor > nota3) {
		menor = nota3;
	}

	cout << "Ingrese nota PC4: ";
	cin >> nota4;
	if (menor > nota4) {
		menor = nota4;
	}

	float promedio = 1.0 * (nota1 + nota2 + nota3 + nota4 - menor) / 3;
	cout << "Promedio : " << promedio;
}

//Problema 3
float obtenerValorIgv(float monto) {
	return 0.18 * monto;
}

void prob03() {
	float precio;
	cout << "Ingrese precio: ";
	cin >> precio;
	cout << "Valor del IGV: " << obtenerValorIgv(precio);
}

float obtenerTotalFactura(float monto) {
	return monto + obtenerValorIgv(monto);
}

//Problema 4
void prob04() {
	float monto;
	cout << "Ingrese subtotal: ";
	cin >> monto;
	cout << "Total factura: " << obtenerTotalFactura(monto);
}


//Problema 5
string obtenerDescripcionCalificacion(float promedioCalificaciones){
	string respuesta = "";
	if(promedioCalificaciones >= 0 && promedioCalificaciones < 3){
		respuesta = "En observacion";
	}else if(promedioCalificaciones >= 3 && promedioCalificaciones < 4.5){
		respuesta = "Bueno";
	}else if(promedioCalificaciones >= 4.5 && promedioCalificaciones <= 5){
		respuesta = "Excelente";
	}else{
		respuesta = "Valor no es correcto";
	}
	return respuesta;
}

void prob05(){
	//valores de prueba
	float valor1 = 0.5, valor2 = 4.3, valor3 = 4.9, valor4 = 9;
	cout << "Promedio: " << valor1 << ". Evaluacion: " << obtenerDescripcionCalificacion(valor1) << endl;
	cout << "Promedio: " << valor2 << ". Evaluacion: " << obtenerDescripcionCalificacion(valor2) << endl;
	cout << "Promedio: " << valor3 << ". Evaluacion: " << obtenerDescripcionCalificacion(valor3) << endl;
	cout << "Promedio: " << valor4 << ". Evaluacion: " << obtenerDescripcionCalificacion(valor4) << endl;
}


//Problema 6
string mostrarDescripcionCategoria(char codCategoria){
	string respuesta;
	switch(codCategoria){
		case 'A': respuesta = "Computo"; break;
		case 'B': respuesta = "TV"; break;
		case 'C': respuesta = "Celulares"; break;
		case 'D': respuesta = "Entretenimiento"; break;
		case 'E': respuesta = "Electrohogar"; break;
		case 'F': respuesta = "Infantil"; break;
		default:  respuesta = "Categoria no es valida";
	}
	return respuesta;
}

void prob06(){
	char cat1 = 'A', cat2 = 'C', cat3 = 'F', cat4 = 'Q';
	cout << "Categoria " << cat1 << ": " << mostrarDescripcionCategoria(cat1) << endl;
	cout << "Categoria " << cat2 << ": " << mostrarDescripcionCategoria(cat2) << endl;
	cout << "Categoria " << cat3 << ": " << mostrarDescripcionCategoria(cat3) << endl;
	cout << "Categoria " << cat4 << ": " << mostrarDescripcionCategoria(cat4) << endl;
}

//Problema 7
float calcularNuevoSalario(float salarioOriginal, int antiguedad){
	float salarioNuevo = salarioOriginal;
	if(antiguedad > 5){
		salarioNuevo *= 1.2;
	}else if(antiguedad >= 1){
		salarioNuevo *= 1.1;
	}
	return salarioNuevo;
}

void prob07(){
	float salario;
	int antiguedad;
	cout << "Ingrese salario: ";
	cin >> salario;
	cout << "Ingrese cantidad de anios de antiguedad: ";
	cin >> antiguedad;
	cout << "Nuevo salario: " << calcularNuevoSalario(salario, antiguedad);
}

//Problema 8
float obtenerPromedioPc(int notas[], int cantidadEvaluaciones){
	int menor = notas[0];
	int suma = 0;
	float promedio = 0;
	for (int i = 0; i < cantidadEvaluaciones ; i++){
		suma += notas[i];
		if(menor > notas[i]){
			menor = notas[i];
		}
	}

	if(cantidadEvaluaciones == 3){
		promedio = 1.0 * suma / 3;
	}else{
		promedio = 1.0 * (suma - menor) / 3;
	}

	return promedio;
}

// Modifica los valores del vector de notas y devuelve la cantidad de notas leida
const int SISTEMA_NO_ENCONTRADO = -1;
int leerVectorNotas(int notas[]){
	char sistema;
	int cantidadNotas = 0;
	cout << "Ingrese sistema de evaluacion: ";
	cin >> sistema;
	if(sistema == 'X'){
		cantidadNotas = 3;
	}else if(sistema == 'Y'){
		cantidadNotas = 4;
	}else{

		cantidadNotas = SISTEMA_NO_ENCONTRADO;
	}

	for(int i = 0; i < cantidadNotas; i++){
		cout << "Ingrese nota PC" << i + 1 << ": ";
		cin >> notas[i];
	}

	return cantidadNotas;

}
void prob08(){

	int notas[4];
	int cantidadNotas = leerVectorNotas(notas);

	if(cantidadNotas == SISTEMA_NO_ENCONTRADO){
		cout << "Valor incorrecto de sistema de evaluacion";
	}else{
		cout << "Promedio: " << obtenerPromedioPc(notas, cantidadNotas);
	}
}

//Problema 9
const int VALOR_NO_CALCULADO = -1;

int calcularNotaMinimaFinal(int practicas[], int cantidadPc, int parcial){
	float promedioPc = obtenerPromedioPc(practicas, cantidadPc);
	float notaFinalCalculada = 30 - parcial - promedioPc;
	int notaFinal = notaFinalCalculada;
	if(notaFinal != notaFinalCalculada){
		notaFinal++;
	}

	if(notaFinal > 20){
		cout << "No es posible aprobar el curso";
		return VALOR_NO_CALCULADO;
	}else if(notaFinal < 0){
		cout << "Alumno ya esta aprobado";
		return VALOR_NO_CALCULADO;
	}

	return notaFinal;
}

void prob09(){
	int practicas[4];
	int parcial;
	int cantidadNotas = leerVectorNotas(practicas);

	if(cantidadNotas == SISTEMA_NO_ENCONTRADO){
		cout << "Valor incorrecto de sistema de evaluacion";
	}else{
		cout << "Ingrese nota Ex. Parcial: ";
		cin >> parcial;
		int final = calcularNotaMinimaFinal(practicas, cantidadNotas, parcial);
		if(final != VALOR_NO_CALCULADO){
			cout << "Alumno se va por: " << calcularNotaMinimaFinal(practicas, cantidadNotas, parcial);
		}
	}
}

//Problema 10
float obtenerPromedioAlumno(int practicas[], int parcial, int final){
	float promedioPracticas = obtenerPromedioPc(practicas, 3);
	return 1.0 * (promedioPracticas + parcial + final) / 3;
}

void leerNotasPc(int notas[]){
	for(int i = 0; i < 3; i++){
		cout << "Ingrese nota PC" << i + 1 << ": ";
		cin >> notas[i];
	}
}

void prob10(){
	int notasA1[3];
	int parcial1;
	int final1;

	int notasA2[3];
	int parcial2;
	int final2;

	cout << "Alumno1" << endl;
	cout << "********" << endl;
	leerNotasPc(notasA1);
	cout << "Ingrese Parcial: ";
	cin >> parcial1;

	cout << "Ingrese Final: ";
	cin >> final1;

	float promedio1 = obtenerPromedioAlumno(notasA1, parcial1, final1);

	cout << endl << "Alumno2" << endl;
	cout << "********" << endl;
	leerNotasPc(notasA2);
	cout << "Ingrese Parcial: ";
	cin >> parcial2;

	cout << "Ingrese Final: ";
	cin >> final2;

	float promedio2 = obtenerPromedioAlumno(notasA2, parcial2, final2);

	if(promedio1 > promedio2){
		cout << "Alumno1 tiene el mayor promedio: " << promedio1;
	}else if(promedio2 > promedio1){
		cout << "Alumno2 tiene el mayor promedio: " << promedio2;
	}else{
		cout << "Alumnos tienen el mismo promedio";
	}


}


int main() {
	//prob01();
	//prob02();
	//prob03();
	//prob04();
	//prob05();
	//prob06();
	//prob07();
	//prob08();
	//prob09();
	prob10();
	return 0;
}

